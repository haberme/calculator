import { h, app } from 'hyperapp'
import './calculator.css'

/*
  the program run a javascript calculator.
  the program uses state machine by using hyperapp.
  please note 3 things:
  1) 0 is not a viable option as 0 in a equation does not mean anything.
  therefor the program blocks the '-'/'+'/'=' keys until a number is pressed.
  2) the program start with positive number but can be modifed to start with
  negatve numberby opening the - key from the begining (testing is required
  after the change).
  3) after the '=' key the program stops until the clean button 'C' is pressed.
*/

const state = {
  total: 0, // the result of the equation.
  show: "", // string represantions of the equation.
  equalPressed: false, // indicate if the '=' key is pressed.

  leftSum : [0], // the number so far exclude the last number right to the operator sign.
  leftSumTotal: 0, // the total sum of the number left of the last operator sign.
  rightSum : 0, // the number right of the operator sign.
  operators: [1], // all the operator sings.
}

// return the current sum of the equation.
function currentSumTotal(state) {
  return state.leftSumTotal + (state.rightSum * state.operators[state.operators.length - 1])
}

// call the delete actions order based on the charecter about to be deleted.
function backspace(state, actions) {
  if ((state.show.charAt(state.show.length - 1) == "+") || (state.show.charAt(state.show.length - 1) == "-")) {
    actions.backspaceOperator()
  } else {
    actions.backspaceNumber()
  }
}

// the actions section
const actions = {
  // the actions that occurred when operatoe key is pressed. 1)sum the current number 2)strat a new right number.
  operation: value => state => ({
    leftSum: state.leftSum.concat(state.rightSum),
    leftSumTotal: currentSumTotal(state),
    operators: state.operators.concat(value[0]),
    show: state.show + value[1],
    rightSum: 0
  }),

  // the actions that occurred when equal key is pressed. calculate the sum and print on screen.
  equal: value => state => ({
    equalPressed: true,
    total: currentSumTotal(state),
    show: state.show + "=" + currentSumTotal(state).toString()
  }),

  // the actions that occurred when number key is pressed. add the number to the right sum.
  number: value => state => ({
    rightSum: state.rightSum * 10 + value,
    showBeforeEqual: state.show,
    show: state.show + value.toString()
  }),

  // the actions that occurred when clear key is pressed. clear everything.
  clear: value => state => ({
    total: 0,
    show: "",
    equalPressed: false,

    leftSum : [0],
    leftSumTotal: 0,
    rightSum : 0,
    operators: [1],
  }),

  // the actions that occurred when number is deleted. delete number from the right sum.
  backspaceNumber: value => state => ({
    show: state.show.slice(0, -1),
    rightSum: Math.floor(state.rightSum / 10),
  }),

  // the actions that occurred when operator is deleted. 1)delete the operator 2)get the previous right sum.
  backspaceOperator: value => state => ({
    show: state.show.slice(0, -1),
    rightSum: state.leftSum[state.leftSum.length - 1],
    leftSumTotal: state.leftSumTotal - (state.leftSum[state.leftSum.length - 1] * state.operators[state.operators.length - 2]),
    leftSum: state.leftSum.slice(0, -1),
    operators: state.operators.slice(0, -1)
  })
}

/*
  the section show that show the numbers keys can be modifed by adding buttons as the ones below with
  the changing the number on the button themself as well as the number send to the actions function.
  Note: if modifed please put every three number in a keypadLine section and separate with separatingLine div
*/
const view = (state, actions) => (
  <main>
    <div className="calculatorBody">
      <h3>{state.show}</h3>
      <div className="separatingLine"></div>
      <div className="keypadLine">
        <button class="b1" onclick={() => actions.clear()}>C</button>
        <button onclick={() => backspace(state, actions)} disabled={state.equalPressed}>-></button>
      </div>
      <div className="separatingLine"></div>
      <div className="keypadLine">
        <button onclick={() => actions.operation([1, "+"])} disabled={(state.rightSum <= 0) || (state.equalPressed)}>+</button>
        <button onclick={() => actions.operation([-1, "-"])} disabled={(state.rightSum <= 0) || (state.equalPressed)}>-</button>
        <button onclick={() => actions.equal()} disabled={(state.rightSum <= 0) || (state.equalPressed)}>=</button>
      </div>
      <div className="separatingLine"></div>
      <div className="keypadLine">
        <button onclick={() => actions.number(1)} disabled={state.equalPressed}>1</button>
        <button onclick={() => actions.number(2)} disabled={state.equalPressed}>2</button>
      </div>
    </div>
  </main>
)

app(state, actions, view, document.body)